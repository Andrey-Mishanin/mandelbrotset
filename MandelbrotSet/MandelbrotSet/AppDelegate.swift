//
//  AppDelegate.swift
//  MandelbrotSet
//
//  Created by Andrey Mishanin on 31/08/2018.
//  Copyright © 2018 Andrey Mishanin. All rights reserved.
//

import Cocoa
import MandelbrotSetCore
import SwiftUI

@NSApplicationMain
final class AppDelegate: NSObject, NSApplicationDelegate, NSWindowDelegate, ViewDelegate {
  var window: NSWindow!
  private var hostingView: NSHostingView<ContentView>!
  
  private var bounds: Bounds! {
    didSet {
      draw()
    }
  }

  func applicationDidFinishLaunching(_ aNotification: Notification) {
    window = NSWindow(contentRect: NSRect(x: 0, y: 0, width: 480, height: 300),
                      styleMask: [.titled, .closable, .miniaturizable, .resizable, .fullSizeContentView],
                      backing: .buffered,
                      defer: false)
    window.center()
    let contentView = ContentView(viewModel: ViewModel(image: nil, opacity: 0.5, animateActivityIndicator: true, windowSize: window.frame.size), delegate: self)
    hostingView = NSHostingView(rootView: contentView)
    window.contentView = hostingView
    window.delegate = self
    window.makeKeyAndOrderFront(nil)
    bounds = Bounds(y: -1..<1, aspectRatio: aspectRatio, midX: 0)
  }

  func receivedDoubleClickAt(unitX: UnitValue, unitY: UnitValue) {
    bounds = bounds.zoomed(into: (x: unitX, y: unitY))
  }

  func clickedZoomOut() {
    bounds = bounds.zoomedOut()
  }

  func clickedZoomIn() {
    bounds = bounds.zoomed(into: (x: 0.5, y: 0.5))
  }

  func windowDidEndLiveResize(_: Notification) {
    if bounds == nil { return }
    bounds = Bounds(y: bounds.y, aspectRatio: aspectRatio, midX: 0)
  }

  func applicationShouldTerminateAfterLastWindowClosed(_ sender: NSApplication) -> Bool {
    return true
  }

  private func draw() {
    let width = Int(window.frame.width)
    let height = Int(window.frame.height)
    let windowDP = DomainParams(bounds: bounds,
                                samples: Samples(x: width, y: height))
    hostingView.rootView = ContentView(viewModel: ViewModel(image: nil, opacity: 0.5, animateActivityIndicator: true, windowSize: window.frame.size), delegate: self)

    let royal: [RGBAColour] = [
      .init(red: 0, green: 0, blue: 0, alpha: .max),
      .init(red: 24, green: 40, blue: 85, alpha: .max),
      .init(red: 121, green: 61, blue: 143, alpha: .max),
      .init(red: 255, green: 228, blue: 133, alpha: .max),
      .white
    ]
    let locations: [UnitValue] = [0, 0.3, 0.6, 0.8, 1]

    let gradient = ArrayGradientPaletteColourStorage(zip(royal, locations).map(ColourWithLocation.init))
//    let gradient = RecursivePaletteColourStorage(colours: royal, numStops: 30)

    let palette = GradientPalette(coloursWithLocations: gradient)
//    let palette = GreyscalePalette()

    let bytesPerRow = width * 4
    let ctx = CGContext(data: nil,
                        width: width,
                        height: height,
                        bitsPerComponent: UInt8.bitWidth,
                        bytesPerRow: bytesPerRow,
                        space: CGColorSpaceCreateDeviceRGB(),
                        bitmapInfo: CGImageAlphaInfo.noneSkipLast.rawValue)!

    let completion = { (_: CGContext) in
      let image = NSImage(cgImage: ctx.makeImage()!,
                          size: CGSize(width: width, height: height))
      self.hostingView.rootView = ContentView(viewModel: ViewModel(image: image, opacity: 1, animateActivityIndicator: false, windowSize: self.window.frame.size), delegate: self)
    }
    
    print(windowDP)
    let numCores = ProcessInfo().activeProcessorCount
    let numPartitions = Int(sqrt(Double(numCores)))
    print("Using \(numPartitions)✕\(numPartitions) partitions across \(numCores) cores")
    let pipeline = ParallelRenderingPipeline(
      domainParams: windowDP,
      output: ctx,
      completion: completion,
      pipelineFactory: { makePipeline(domainParams: $0, output: $1, completion: $2, palette: palette) },
      numPartitionsX: numPartitions,
      numPartitionsY: numPartitions,
      callCompletionWithIntermediateResults: true
    )
    pipeline.run()
  }

  private var width: Int { return Int(window.frame.width) }
  private var height: Int { return Int(window.frame.height) }
  private var aspectRatio: Double { return Double(width) / Double(height) }
}

private func makePipeline<P>(domainParams: DomainParams,
                             output: PartialRenderOutput<CGContext>,
                             completion: @escaping (PartialRenderOutput<CGContext>) -> Void,
                             palette: P) -> RenderingPipeline<P, PartialRenderOutput<CGContext>> where P: Palette {
  return RenderingPipeline(
    domainParams: domainParams,
    palette: palette,
    output: output,
    completion: completion,
    renderRunner: { DispatchQueue.global().async(execute: $0) },
    completionRunner: { DispatchQueue.main.async(execute: $0) })
}

extension CGContext: RenderOutput {
  public func set(colour: RGBAColour, at index: Int) {
    let colourData = data!.assumingMemoryBound(to: RGBAColour.self)
    colourData[index] = colour
  }
}
