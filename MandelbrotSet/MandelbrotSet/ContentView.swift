//
//  ContentView.swift
//  MandelbrotSet
//
//  Created by Andrey Mishanin on 26/04/2020.
//  Copyright © 2020 Andrey Mishanin. All rights reserved.
//

import SwiftUI
import MandelbrotSetCore

protocol ViewDelegate: class {
  func receivedDoubleClickAt(unitX: UnitValue, unitY: UnitValue)
  func clickedZoomOut()
  func clickedZoomIn()
}

struct ViewModel {
  let image: NSImage?
  let opacity: CGFloat
  let animateActivityIndicator: Bool
  let windowSize: CGSize
}

struct ActivityIndicatorView: NSViewRepresentable {
  let isAnimating: Bool

  func makeNSView(context: Context) -> NSProgressIndicator {
    let i = NSProgressIndicator()
    i.style = .spinning
    i.isDisplayedWhenStopped = false
    return i
  }

  func updateNSView(_ nsView: NSProgressIndicator, context: Context) {
    if isAnimating {
      nsView.startAnimation(nil)
    } else {
      nsView.stopAnimation(nil)
    }
  }
}

struct TapOverlay: NSViewRepresentable {
  class Coordinator: NSObject {
    var tapHandler: (CGPoint) -> Void

    init(tapHandler: @escaping (CGPoint) -> Void) {
      self.tapHandler = tapHandler
    }

    @objc func tapped(recognizer: NSClickGestureRecognizer) {
      let point = recognizer.location(in: recognizer.view)
      tapHandler(point)
    }
  }

  let tapHandler: (CGPoint) -> Void

  func makeNSView(context: Context) -> NSView {
    let v = NSView(frame: .zero)
    let gr = NSClickGestureRecognizer(target: context.coordinator, action: #selector(Coordinator.tapped(recognizer:)))
    gr.numberOfClicksRequired = 2
    v.addGestureRecognizer(gr)
    return v
  }

  func updateNSView(_: NSView, context: Context) {
    context.coordinator.tapHandler = tapHandler
  }

  func makeCoordinator() -> Coordinator {
    Coordinator(tapHandler: tapHandler)
  }
}

struct ContentView: View {
  let viewModel: ViewModel
  unowned let delegate: ViewDelegate

  var body: some View {
    ZStack {
      ZStack {
        if viewModel.image != nil {
          Image(nsImage: viewModel.image!)
            .resizable()
            .opacity(Double(viewModel.opacity))
        }
        ActivityIndicatorView(isAnimating: viewModel.animateActivityIndicator)
      }
      .overlay(TapOverlay(tapHandler: { location in
        let unitX = UnitValue(Double(location.x / self.viewModel.windowSize.width))
        let unitY = UnitValue(Double((self.viewModel.windowSize.height - location.y) / self.viewModel.windowSize.height))
        self.delegate.receivedDoubleClickAt(unitX: unitX, unitY: unitY)
      }))
      VStack {
        Spacer()
        HStack(spacing: 0) {
          Spacer()
          Button(action: { self.delegate.clickedZoomIn() }) {
            Text("+")
          }
          Button(action: { self.delegate.clickedZoomOut() }) {
            Text("−")
          }
        }
      .padding()
      }
    }
    .frame(maxWidth: .infinity, maxHeight: .infinity)
  }
}
