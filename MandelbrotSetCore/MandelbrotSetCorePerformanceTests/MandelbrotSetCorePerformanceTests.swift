//
//  MandelbrotSetCorePerformanceTests.swift
//  MandelbrotSetCorePerformanceTests
//
//  Created by Andrey Mishanin on 27/12/2018.
//  Copyright © 2018 Andrey Mishanin. All rights reserved.
//

import XCTest
import MandelbrotSetCore

final class MandelbrotSetCorePerformanceTests: XCTestCase {
  func test_SingleThreaded() {
    let pipeline = RenderingPipeline(domainParams: windowDP,
                                     palette: palette,
                                     output: ColourOutput(width: width, height: height),
                                     completion: { _ in },
                                     renderRunner: { $0() },
                                     completionRunner: { $0() })

    measure {
      pipeline.run()
    }
  }

  func test_Parallel() {
    let numCores = ProcessInfo().activeProcessorCount
    let numPartitions = Int(sqrt(Double(numCores)))
    let s = DispatchSemaphore(value: 0)
    let pipeline = ParallelRenderingPipeline(domainParams: windowDP,
                                             output: ColourOutput(width: width, height: height),
                                             completion: { _ in s.signal() },
                                             pipelineFactory: workerFactory,
                                             numPartitionsX: numPartitions,
                                             numPartitionsY: numPartitions,
                                             callCompletionWithIntermediateResults: false)

    measure {
      pipeline.run()
      s.wait()
    }
  }
}

private let width = 5120
private let height = 2880
private let aspectRatio = Double(width) / Double(height)
private let bounds = Bounds(y: -1..<1, aspectRatio: aspectRatio, midX: 0)
private let windowDP = DomainParams(bounds: bounds,
                            samples: Samples(x: width, y: height))
private let forestFire: [RGBAColour] = [
  .init(red: 44, green: 44, blue: 46, alpha: .max),
  .init(red: 184, green: 44, blue: 28, alpha: .max),
  .init(red: 255, green: 126, blue: 43, alpha: .max),
  .init(red: 255, green: 204, blue: 111, alpha: .max),
  .init(red: 243, green: 251, blue: 159, alpha: .max),
]
private let gradient = RecursivePaletteColourStorage(colours: forestFire, numStops: 30)
private let palette = GradientPalette(coloursWithLocations: gradient)
private let completionQueue = DispatchQueue(label: "CompletionQueue")

private func workerFactory(domainParams: DomainParams,
                           output: PartialRenderOutput<ColourOutput>,
                           completion: @escaping (PartialRenderOutput<ColourOutput>) -> Void) -> RenderingPipeline<GradientPalette<RecursivePaletteColourStorage>, PartialRenderOutput<ColourOutput>> {
  return RenderingPipeline(domainParams: domainParams,
                           palette: palette,
                           output: output,
                           completion: completion,
                           renderRunner: { DispatchQueue.global().async(execute: $0) },
                           // Using private serial queue for completions so the main thread can safely wait on the
                           // semaphore
                           completionRunner: { completionQueue.async(execute: $0) })
}

private final class ColourOutput: RenderOutput {
  let width: Int
  let height: Int

  init(width: Int, height: Int) {
    self.width = width
    self.height = height
  }

  func set(colour: RGBAColour, at index: Int) { }
}

final class DoubleArrayPerformanceTests: XCTestCase {
  private let numElements = 10_000_000
  private let elementRange = 0.0..<1.0
  private let limit = 0.5

  func test_Limit() {
    let a = (0..<numElements).map { _ in Double.random(in: elementRange) }

    measure {
      let _ = a.map { $0 < limit ? -limit : limit }
    }
  }

  func test_LimitContiguous() {
    let a = ContiguousArray<Double>((0..<numElements).map { _ in Double.random(in: elementRange) })

    measure {
      let _ = a.map { $0 < limit ? -limit : limit }
    }
  }

  func test_LimitVectorized() {
    let a = (0..<numElements).map { _ in Double.random(in: elementRange) }

    measure {
      let _ = a.limit(to: limit, replacementValue: limit)
    }
  }

  func test_LimitContiguousVectorized() {
    let a = ContiguousArray<Double>((0..<numElements).map { _ in Double.random(in: elementRange) })

    measure {
      let _ = a.limit(to: limit, replacementValue: limit)
    }
  }
}
