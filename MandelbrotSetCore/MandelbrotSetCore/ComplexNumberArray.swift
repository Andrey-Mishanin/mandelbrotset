//
//  ComplexNumberArray.swift
//  MandelbrotSetCore
//
//  Created by Andrey Mishanin on 06/01/2019.
//  Copyright © 2019 Andrey Mishanin. All rights reserved.
//

import Accelerate

public struct ComplexNumberArray: Equatable {
  private(set) var reals: [Double]
  private(set) var ims: [Double]

  public init(_ values: [ℂ]) {
    self.init(reals: values.map { $0.real },
              ims: values.map { $0.imaginary })
  }

  init(reals: [Double], ims: [Double]) {
    self.reals = reals
    self.ims = ims
  }

  var count: Int {
    return reals.count
  }
}

public extension ComplexNumberArray {
  mutating func asDSP(_ body: (inout DSPDoubleSplitComplex) -> Void) {
    reals.withUnsafeMutableBufferPointer { realp in
      ims.withUnsafeMutableBufferPointer { imagp in
        var c = DSPDoubleSplitComplex(realp: realp.baseAddress!,
                                      imagp: imagp.baseAddress!)
        body(&c)
      }
    }
  }
}

public extension ComplexNumberArray {
  static func +(lhs: ComplexNumberArray, rhs: ComplexNumberArray) -> ComplexNumberArray {
    var _lhs = lhs
    var _rhs = rhs
    return apply2(&_lhs, &_rhs) { vDSP_zvaddD(&$0, 1, &$1, 1, &$2, 1, $3) }
  }

  static func *(lhs: ComplexNumberArray, rhs: ComplexNumberArray) -> ComplexNumberArray {
    var _lhs = lhs
    var _rhs = rhs
    return apply2(&_lhs, &_rhs) { vDSP_zvmulD(&$0, 1, &$1, 1, &$2, 1, $3, 1) }
  }

  static func multiplyAdd(_ a: ComplexNumberArray, _ b: ComplexNumberArray, _ c: ComplexNumberArray) -> ComplexNumberArray {
    var _a = a
    var _b = b
    var _c = c
    return apply3(&_a, &_b, &_c) { vDSP_zvmaD(&$0, 1, &$1, 1, &$2, 1, &$3, 1, $4) }
  }

  mutating func squareAdd(_ c: ComplexNumberArray) {
    precondition(count == c.count)
    var _a = self
    var _c = c
    _a.asDSP { __a in
      _c.asDSP { __c in
        vDSP_zvmaD(&__a, 1, &__a, 1, &__c, 1, &__a, 1, vDSP_Length(count))
      }
    }
    self = _a
  }

  func magnitudes() -> [Double] {
    var res = Array<Double>(repeating: 0, count: count)
    var _a = self
    _a.asDSP { a in
      vDSP_zvmagsD(&a, 1, &res, 1, vDSP_Length(count))
    }
    return res
  }

  private typealias ApplyOp2 = (
    inout DSPDoubleSplitComplex,
    inout DSPDoubleSplitComplex,
    inout DSPDoubleSplitComplex,
    vDSP_Length) -> Void
  private static func apply2(_ lhs: inout ComplexNumberArray,
                             _ rhs: inout ComplexNumberArray,
                             _ op: ApplyOp2) -> ComplexNumberArray {
    precondition(lhs.count == rhs.count)
    let count = lhs.count
    var resReal = Array<Double>(repeating: 0, count: count)
    var resIm = Array<Double>(repeating: 0, count: count)
    var res = DSPDoubleSplitComplex(realp: &resReal, imagp: &resIm)
    lhs.asDSP { a in
      rhs.asDSP { b in
        op(&a, &b, &res, vDSP_Length(count))
      }
    }
    return ComplexNumberArray(reals: resReal, ims: resIm)
  }

  private typealias ApplyOp3 = (
    inout DSPDoubleSplitComplex,
    inout DSPDoubleSplitComplex,
    inout DSPDoubleSplitComplex,
    inout DSPDoubleSplitComplex,
    vDSP_Length) -> Void
  private static func apply3(_ a: inout ComplexNumberArray,
                             _ b: inout ComplexNumberArray,
                             _ c: inout ComplexNumberArray,
                             _ op: ApplyOp3) -> ComplexNumberArray {
    precondition(a.count == b.count)
    precondition(a.count == c.count)
    let count = a.count
    var resReal = Array<Double>(repeating: 0, count: count)
    var resIm = Array<Double>(repeating: 0, count: count)
    var res = DSPDoubleSplitComplex(realp: &resReal, imagp: &resIm)
    a.asDSP { _a in
      b.asDSP { _b in
        c.asDSP { _c in
          op(&_a, &_b, &_c, &res, vDSP_Length(count))
        }
      }
    }
    return ComplexNumberArray(reals: resReal, ims: resIm)
  }
}
