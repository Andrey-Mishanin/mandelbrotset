//
//  ASCIIPalette.swift
//  MandelbrotSetCore
//
//  Created by Andrey Mishanin on 29/08/2018.
//  Copyright © 2018 Andrey Mishanin. All rights reserved.
//

public struct ASCIIPalette: Palette {
  public static var cardinality: Int {
    return gradient.count
  }

  public init() {}

  public func colorise(_ x: Int) -> Character {
    return gradient[x - 1]
  }
}

private let gradient = Array("·.,:;|!([$O0*%#@")
