//
//  GreyscalePalette.swift
//  MandelbrotSetCore
//
//  Created by Andrey Mishanin on 04/09/2018.
//  Copyright © 2018 Andrey Mishanin. All rights reserved.
//

public struct GreyscalePalette: Palette {
  public init() { }

  public static var cardinality: Int {
    return Int(UInt8.max)
  }

  public func colorise(_ x: Int) -> RGBAColour {
    return RGBAColour(grey: UInt8(x - 1))
  }
}
