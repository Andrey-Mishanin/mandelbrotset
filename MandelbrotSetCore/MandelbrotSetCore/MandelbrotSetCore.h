//
//  MandelbrotSetCore.h
//  MandelbrotSetCore
//
//  Created by Andrey Mishanin on 29/08/2018.
//  Copyright © 2018 Andrey Mishanin. All rights reserved.
//

#import <Cocoa/Cocoa.h>

//! Project version number for MandelbrotSetCore.
FOUNDATION_EXPORT double MandelbrotSetCoreVersionNumber;

//! Project version string for MandelbrotSetCore.
FOUNDATION_EXPORT const unsigned char MandelbrotSetCoreVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <MandelbrotSetCore/PublicHeader.h>


