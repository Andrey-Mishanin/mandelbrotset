//
//  RenderingPipeline.swift
//  MandelbrotSetCore
//
//  Created by Andrey Mishanin on 23/09/2018.
//  Copyright © 2018 Andrey Mishanin. All rights reserved.
//

public typealias RenderRunner = (@escaping () -> Void) -> Void
public typealias CompletionRunner = (@escaping () -> Void) -> Void

public protocol RenderingPipelineProtocol: class {
  associatedtype P: Palette
  associatedtype Output: RenderOutput where P.Colour == Output.Colour
  func run()
}

public final class RenderingPipeline<P, Output>: RenderingPipelineProtocol where P: Palette, Output: RenderOutput, P.Colour == Output.Colour {
  public typealias Completion = (Output) -> Void

  private let domain: [[ℂ]]
  private let domainParams: DomainParams
  private let palette: P
  private let output: Output
  private let completion: Completion
  private let renderRunner: RenderRunner
  private let completionRunner: CompletionRunner

  public init(domainParams: DomainParams,
              palette: P,
              output: Output,
              completion: @escaping Completion,
              renderRunner: @escaping RenderRunner,
              completionRunner: @escaping CompletionRunner) {
    precondition(domainParams.samples.y == output.height)
    precondition(domainParams.samples.x == output.width)
    self.domainParams = domainParams
    self.palette = palette
    self.output = output
    self.completion = completion
    self.renderRunner = renderRunner
    self.completionRunner = completionRunner

    domain = makeDomain(domainParams)
  }

  public func run() {
    renderRunner {
      self.doRender()
    }
  }

  private func doRender() {
    let renderer = ImperativeRenderer(palette: palette)
    let flatDomain = ComplexNumberArray(domain.flatMap { $0 })

    let xs = renderer.render(flatDomain)
    let colours = xs.map(palette.colorise)

    for x in 0..<output.width {
      for y in 0..<output.height {
        let flatIdx = y * output.width + x
        output.set(colour: colours[flatIdx], at: flatIdx)
      }
    }

    completionRunner {
      self.completion(self.output)
    }
  }
}
