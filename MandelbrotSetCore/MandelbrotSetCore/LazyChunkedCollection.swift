//
//  LazyChunkedCollection.swift
//  MandelbrotSetCore
//
//  Created by Andrey Mishanin on 10/10/2018.
//  Copyright © 2018 Andrey Mishanin. All rights reserved.
//

public struct LazyChunkedCollection<Base: Collection>: Collection {
  public typealias Index = Base.Index
  public typealias Element = Base.SubSequence

  private let base: Base
  private let chunkSize: Int

  public init(base: Base, chunkSize: Int) {
    self.base = base
    self.chunkSize = chunkSize
  }

  public var startIndex: Index { return base.startIndex }
  public var endIndex: Index { return base.endIndex }
  public subscript(i: Index) -> Element {
    return base[i..<index(after: i)]
  }
  public func index(after i: Index) -> Index {
    return base.index(i, offsetBy: chunkSize, limitedBy: endIndex) ?? endIndex
  }
}

public extension LazyCollectionProtocol {
  func chunked(size: Int) -> LazyChunkedCollection<Self> {
    return LazyChunkedCollection(base: self, chunkSize: size)
  }
}
