//
//  RenderOutput.swift
//  MandelbrotSetCore
//
//  Created by Andrey Mishanin on 23/09/2018.
//  Copyright © 2018 Andrey Mishanin. All rights reserved.
//

public protocol RenderOutput: class {
  associatedtype Colour

  var width: Int { get }
  var height: Int { get }
  func set(colour: Colour, at index: Int)
}
