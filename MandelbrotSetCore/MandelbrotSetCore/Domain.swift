//
//  Domain.swift
//  MandelbrotSetCore
//
//  Created by Andrey Mishanin on 29/08/2018.
//  Copyright © 2018 Andrey Mishanin. All rights reserved.
//

public struct Bounds: Equatable {
  public let x: Range<Double>
  public let y: Range<Double>

  public init(x: Range<Double>, y: Range<Double>) {
    self.x = x
    self.y = y
  }
}

extension Bounds: CustomDebugStringConvertible {
  public var debugDescription: String {
    return "{x: \(x), y: \(y)}"
  }
}

public extension Bounds {
  init(y: Range<Double>, aspectRatio: Double, midX: Double) {
    self.init(x: Range(midPoint: midX, span: y.span * aspectRatio),
              y: y)
  }
}

public extension Bounds {
  func zoomed(into unitPoint: (x: UnitValue, y: UnitValue)) -> Bounds {
    return Bounds(y: Range(midPoint: y.value(atFraction: unitPoint.y),
                           span: y.span / 2),
                  aspectRatio: x.span / y.span,
                  midX: x.value(atFraction: unitPoint.x))
  }

  func zoomedOut() -> Bounds {
    return Bounds(x: Range(midPoint: x.midPoint, span: x.span * 2),
                  y: Range(midPoint: y.midPoint, span: y.span * 2))
  }
}

public struct Samples: Equatable {
  public let x: Int
  public let y: Int

  public init(x: Int, y: Int) {
    self.x = x
    self.y = y
  }
}

extension Samples: CustomDebugStringConvertible {
  public var debugDescription: String {
    return "{x: \(x), y: \(y)}"
  }
}

public struct DomainParams: Equatable {
  public let bounds: Bounds
  public let samples: Samples

  public init(bounds: Bounds, samples: Samples) {
    self.bounds = bounds
    self.samples = samples
  }

  public func partitionedInto(numX: Int, numY: Int) -> [[DomainParams]] {
    let yPartitionSpan = bounds.y.span / Double(numY)
    let yLowerBounds = stride(from: bounds.y.lowerBound,
                              to: bounds.y.upperBound,
                              by: yPartitionSpan)
    let xPartitionSpan = bounds.x.span / Double(numX)
    let xLowerBounds = stride(from: bounds.x.lowerBound,
                              to: bounds.x.upperBound,
                              by: xPartitionSpan)
    let partitionBounds = yLowerBounds.map { y in
      xLowerBounds.map { x in
        Bounds(x: Range(lowerBound: x, span: xPartitionSpan),
               y: Range(lowerBound: y, span: yPartitionSpan))
      }
    }
    let partitionSamples = (0..<numY).map { _ in
      (0..<numX).map { _ in
        Samples(x: samples.x / numX, y: samples.y / numY)
      }
    }
    return zip(partitionBounds, partitionSamples).map {
      zip($0.0, $0.1).map {
        DomainParams(bounds: $0.0, samples: $0.1)
      }
    }
  }
}

public func makeDomain(_ params: DomainParams) -> [[ℂ]] {
  let sideX = params.bounds.x.makeStride(count: params.samples.x)
  let sideY = params.bounds.y.makeStride(count: params.samples.y)

  return sideY.map { y in sideX.map { x in ℂ(x, i: y) } }
}

private extension Range where Bound == Double {
  func makeStride(count: Int) -> StrideTo<Bound> {
    return stride(from: lowerBound,
                  to: upperBound,
                  by: (upperBound - lowerBound) / Double(count))
  }
}
