//
//  GradientPalette.swift
//  MandelbrotSetCore
//
//  Created by Andrey Mishanin on 04/09/2018.
//  Copyright © 2018 Andrey Mishanin. All rights reserved.
//

public struct ColourWithLocation: Equatable {
  let colour: RGBAColour
  let location: UnitValue

  public init(colour: RGBAColour, location: UnitValue) {
    self.colour = colour
    self.location = location
  }
}

public struct GradientPalette<ColourStorage: GradientPaletteColourStorage>: Palette {
  private let coloursWithLocations: ColourStorage
  private let lastValidIdx: ColourStorage.Index
  private let lastActiveRange: (ColourWithLocation, ColourWithLocation)

  public static var cardinality: Int {
    return Int(UInt8.max) + 1
  }

  public init(coloursWithLocations: ColourStorage) {
    precondition(coloursWithLocations.count >= 2)
    self.coloursWithLocations = coloursWithLocations
    lastValidIdx = coloursWithLocations.index(before: coloursWithLocations.endIndex)
    lastActiveRange = (coloursWithLocations[coloursWithLocations.index(before: lastValidIdx)],
                       coloursWithLocations[lastValidIdx])
  }

  public func colorise(_ x: Int) -> RGBAColour {
    // vDSP_vsdiv
    let fraction = UnitValue(Double(x - 1) / Double(GradientPalette.cardinality - 1))
    let activeRange = findActiveRange(for: fraction)
    // vDSP_vsub
    let activeRangeLocationSpan = activeRange.1.location - activeRange.0.location
    // vDSP_svdiv to get 1/activeRangeLocationSpan + vDSP_vsbm
    let fractionTranslatedToActiveRange = (fraction - activeRange.0.location) / activeRangeLocationSpan
    // vDSP_vintb?
    return RGBAColour.interpolate(fraction: fractionTranslatedToActiveRange,
                                  between: activeRange.0.colour,
                                  and: activeRange.1.colour)
  }

  private func findActiveRange(for fraction: UnitValue) -> (ColourWithLocation, ColourWithLocation) {
    guard (fraction.value < 1) else {
      return lastActiveRange
    }

    let activeRangeLowerIdx = coloursWithLocations.activeRangeLowerIndex(for: fraction)

    if activeRangeLowerIdx < lastValidIdx {
      let activeRangeUpperIdx = coloursWithLocations.index(after: activeRangeLowerIdx)
      return (coloursWithLocations[activeRangeLowerIdx], coloursWithLocations[activeRangeUpperIdx])
    } else {
      return lastActiveRange
    }
  }
}
