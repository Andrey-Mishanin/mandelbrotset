//
//  RGBAColor.swift
//  MandelbrotSetCore
//
//  Created by Andrey Mishanin on 04/09/2018.
//  Copyright © 2018 Andrey Mishanin. All rights reserved.
//

public struct RGBAColour: Equatable {
  public let red: UInt8
  public let green: UInt8
  public let blue: UInt8
  public let alpha: UInt8

  public init(red: UInt8, green: UInt8, blue: UInt8, alpha: UInt8) {
    self.red = red
    self.green = green
    self.blue = blue
    self.alpha = alpha
  }

  public init(grey: UInt8) {
    self.init(red: grey, green: grey, blue: grey, alpha: .max)
  }
}

public extension RGBAColour {
  static func interpolate(fraction: UnitValue, between c1: RGBAColour, and c2: RGBAColour) -> RGBAColour {
    let r = interpolateFraction(fraction, between: c1.red, and: c2.red)
    let g = interpolateFraction(fraction, between: c1.green, and: c2.green)
    let b = interpolateFraction(fraction, between: c1.blue, and: c2.blue)
    return RGBAColour(red: r, green: g, blue: b, alpha: .max)
  }
}

private func interpolateFraction(_ fraction: UnitValue, between component1: UInt8, and component2: UInt8) -> UInt8 {
  return UInt8(Double(component1) + fraction * (Double(component2) - Double(component1)))
}

public extension RGBAColour {
  static let red = RGBAColour(red: .max, green: 0, blue: 0, alpha: .max)
  static let blue = RGBAColour(red: 0, green: 0, blue: .max, alpha: .max)
  static let white = RGBAColour(grey: .max)
  static let black = RGBAColour(grey: 0)
}
