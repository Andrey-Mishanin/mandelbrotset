//
//  Renderer.swift
//  MandelbrotSetCore
//
//  Created by Andrey Mishanin on 29/08/2018.
//  Copyright © 2018 Andrey Mishanin. All rights reserved.
//

public protocol Renderer {
  associatedtype P: Palette

  init(palette: P)

  func render(_ c: ComplexNumberArray) -> [Int]
}

public struct TracebackInfo {
  let series: [ℂ]
  let magnitudes: [Double]
  let result: Int
}
