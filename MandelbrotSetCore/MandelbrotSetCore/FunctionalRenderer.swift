//
//  FunctionalRenderer.swift
//  MandelbrotSetCore
//
//  Created by Andrey Mishanin on 20/12/2018.
//  Copyright © 2018 Andrey Mishanin. All rights reserved.
//

public struct FunctionalRenderer<P: Palette>: Renderer {
  private let palette: P

  public init(palette: P) {
    self.palette = palette
  }

  private func render(_ c: ℂ) -> Int {
    let maxIter = type(of: palette).cardinality
    return sequence(state: (ℂ(0), c), next: { state -> ℂ? in
      let (z, c) = state
      guard z.magnitude <= 4 else { return nil }
      state = (z * z + c, c)
      return z
    }).lazy.prefix__(maxIter).count()
  }

  public func render(_ c: ComplexNumberArray) -> [Int] {
    return zip(c.reals, c.ims).map({ ℂ($0, i: $1) }).map(render)
  }
}

private struct PrefixSequence<Base: IteratorProtocol>: IteratorProtocol, Sequence {
  private var base: Base
  private var count: Int

  @inlinable
  init(base: Base, maxLength: Int) {
    self.base = base
    self.count = maxLength
  }

  @inlinable
  mutating func next() -> Base.Element? {
    count -= 1
    return count >= 0 ? base.next() : nil
  }
}

private struct _ClosureBasedIterator<Element> : IteratorProtocol, Sequence {
  @inlinable
  internal init(_ body: @escaping () -> Element?) {
    self._body = body
  }
  @inlinable
  internal func next() -> Element? { return _body() }
  @usableFromInline
  internal let _body: () -> Element?
}

private extension Sequence {
  // prefix(_:) from stdlib has to return type-erased AnySequence, which is an optimization barrier and should be avoided
  // See https://forums.swift.org/t/rationalizing-sequence-subsequence/17586 for more details on why.

//  func prefix__(_ maxLength: Int) -> PrefixSequence<Iterator> {
//    return PrefixSequence(base: self.makeIterator(), maxLength: maxLength)
//  }

  // However, this type-erased sequence performs even better (?!) than PrefixSequence
  // See https://forums.swift.org/t/troubling-sequence/13130/2 for more details
  func prefix__(_ maxLength: Int) -> _ClosureBasedIterator<Iterator.Element> {
    var it = makeIterator()
    var count = maxLength
    return _ClosureBasedIterator {
      count -= 1
      return count >= 0 ? it.next() : nil
    }
  }

  @inline(__always)
  func count() -> Int {
    return reduce(into: 0, { i, _ in i += 1 })
  }
}
