//
//  RecursivePaletteColourStorage.swift
//  MandelbrotSetCore
//
//  Created by Andrey Mishanin on 19/09/2018.
//  Copyright © 2018 Andrey Mishanin. All rights reserved.
//

public struct RecursivePaletteColourStorage: GradientPaletteColourStorage {
  public typealias Index = Int

  private let colours: [RGBAColour]
  private let numStops: Int

  public init(colours: [RGBAColour], numStops: Int) {
    self.colours = colours
    self.numStops = numStops
  }

  public var startIndex: Int { return 0 }

  public var endIndex: Int { return numStops }

  public func index(after i: Int) -> Int { return i + 1 }

  public func index(before i: Int) -> Int { return i - 1 }
  
  public subscript(idx: Int) -> ColourWithLocation {
    let colour = colours[idx % colours.count]
    if idx == index(before: endIndex) {
      return .init(colour: colour, location: 1)
    }
    let location = 1 - pow(2, Double(-idx))
    return .init(colour: colour, location: UnitValue(location))
  }

  public func activeRangeLowerIndex(for fraction: UnitValue) -> Int {
    return Int(floor(-log2((1 - fraction).value)))
  }
}
