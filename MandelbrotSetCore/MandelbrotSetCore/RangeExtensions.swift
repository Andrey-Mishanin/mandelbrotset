//
//  RangeExtensions.swift
//  MandelbrotSetCore
//
//  Created by Andrey Mishanin on 04/09/2018.
//  Copyright © 2018 Andrey Mishanin. All rights reserved.
//

public extension Range where Bound == Double {
  init(midPoint: Double, span: Double) {
    self.init(uncheckedBounds: (midPoint - span / 2,
                                midPoint + span / 2))
  }

  init(lowerBound: Double, span: Double) {
    self.init(uncheckedBounds: (lowerBound, lowerBound + span))
  }

  var span: Double {
    return upperBound - lowerBound
  }

  func value(atFraction fraction: UnitValue) -> Double {
    return lowerBound + fraction * span
  }

  var midPoint: Double {
    return value(atFraction: 0.5)
  }
}
