//
//  Palette.swift
//  MandelbrotSetCore
//
//  Created by Andrey Mishanin on 29/08/2018.
//  Copyright © 2018 Andrey Mishanin. All rights reserved.
//

public protocol Palette {
  associatedtype Colour

  static var cardinality: Int { get }

  // 1 <= x <= cardinality
  func colorise(_ x: Int) -> Colour
}
