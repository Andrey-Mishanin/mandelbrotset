//
//  ComplexNumber.swift
//  MandelbrotSetCore
//
//  Created by Andrey Mishanin on 29/08/2018.
//  Copyright © 2018 Andrey Mishanin. All rights reserved.
//

public struct ComplexNumber: Equatable {
  public let real: Double
  public let imaginary: Double

  public init(_ real: Double, i imaginary: Double) {
    self.real = real
    self.imaginary = imaginary
  }

  public init(_ real: Double) {
    self.init(real, i: 0)
  }

  public init(i imaginary: Double) {
    self.init(0, i: imaginary)
  }

  public init() {
    self.init(0, i: 0)
  }

  public var magnitude: Double {
    return real * real + imaginary * imaginary
  }
}

public func +(x: ℂ, y: ℂ) -> ℂ {
  return ℂ(x.real + y.real, i: x.imaginary + y.imaginary)
}

public func *(x: ℂ, y: ℂ) -> ℂ {
  let (a, b, c, d) = (x.real, x.imaginary, y.real, y.imaginary)
  return ℂ(a * c - b * d, i: b * c + a * d)
}

public typealias ℂ = ComplexNumber
