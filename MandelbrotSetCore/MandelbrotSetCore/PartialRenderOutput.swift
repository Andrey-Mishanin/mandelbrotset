//
//  PartialRenderOutput.swift
//  MandelbrotSetCore
//
//  Created by Andrey Mishanin on 23/12/2018.
//  Copyright © 2018 Andrey Mishanin. All rights reserved.
//

import Foundation

public final class PartialRenderOutput<Base: RenderOutput>: RenderOutput {
  public typealias Colour = Base.Colour

  public let width: Int
  public let height: Int

  public let offsetX: Int
  public let offsetY: Int
  public let base: Base

  public init(base: Base, offsetX: Int, offsetY: Int, width: Int, height: Int) {
    self.base = base
    self.offsetX = offsetX
    self.offsetY = offsetY
    self.width = width
    self.height = height
  }

  public func set(colour: Colour, at index: Int) {
    let x = index % width
    let y = index / width
    let baseX = x + offsetX
    let baseY = y + offsetY
    let flatIdx = baseY * base.width + baseX
    base.set(colour: colour, at: flatIdx)
  }
}

extension RenderOutput {
  public func partitionInto(numX: Int, numY: Int) -> [[PartialRenderOutput<Self>]] {
    let partitionWidth = width / numX
    let partitionHeight = height / numY
    let offsetsX = stride(from: 0, to: width, by: partitionWidth)
    let offsetsY = stride(from: 0, to: height, by: partitionHeight)

    return offsetsY.map { y in
      offsetsX.map { x in
        PartialRenderOutput(base: self, offsetX: x, offsetY: y, width: partitionWidth, height: partitionHeight)
      }
    }
  }
}

extension PartialRenderOutput: Equatable {
  public static func ==(lhs: PartialRenderOutput<Base>, rhs: PartialRenderOutput<Base>) -> Bool {
    return lhs.base === rhs.base &&
      lhs.offsetX == rhs.offsetX &&
      lhs.offsetY == rhs.offsetY &&
      lhs.width == rhs.width &&
      lhs.height == rhs.height
  }
}
