//
//  DoubleArrayExtensions.swift
//  MandelbrotSetCore
//
//  Created by Andrey Mishanin on 20/01/2019.
//  Copyright © 2019 Andrey Mishanin. All rights reserved.
//

import Accelerate

public extension Array where Element == Double {
  func limit(to: Double, replacementValue: Double) -> [Double] {
    var a = self
    var _to = to
    var _r = replacementValue
    var res = Array<Double>(repeating: 0, count: count)
    a.withUnsafeMutableBufferPointer { _a in
      vDSP_vlimD(_a.baseAddress!, 1, &_to, &_r, &res, 1, vDSP_Length(count))
    }
    return res
  }

  func threshold(limit: Double) -> [Double] {
    var a = self
    var _l = limit
    var res = Array<Double>(repeating: 0, count: count)
    a.withUnsafeMutableBufferPointer { _a in
      vDSP_vthrD(_a.baseAddress!, 1, &_l, &res, 1, vDSP_Length(count))
    }
    return res
  }
}

public extension ContiguousArray where Element == Double {
  func limit(to: Double, replacementValue: Double) -> [Double] {
    var a = self
    var _to = to
    var _r = replacementValue
    var res = Array<Double>(repeating: 0, count: count)
    a.withUnsafeMutableBufferPointer { _a in
      vDSP_vlimD(_a.baseAddress!, 1, &_to, &_r, &res, 1, vDSP_Length(count))
    }
    return res
  }

  func threshold(limit: Double) -> [Double] {
    var a = self
    var _l = limit
    var res = Array<Double>(repeating: 0, count: count)
    a.withUnsafeMutableBufferPointer { _a in
      vDSP_vthrD(_a.baseAddress!, 1, &_l, &res, 1, vDSP_Length(count))
    }
    return res
  }
}
