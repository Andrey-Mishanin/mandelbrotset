//
//  UnitValue.swift
//  MandelbrotSetCore
//
//  Created by Andrey Mishanin on 08/09/2018.
//  Copyright © 2018 Andrey Mishanin. All rights reserved.
//

public struct UnitValue: Comparable {
  public let value: Double

  public init(_ value: Double) {
    precondition(value >= 0 && value <= 1)
    self.value = value
  }

  public static func < (lhs: UnitValue, rhs: UnitValue) -> Bool { return lhs.value < rhs.value }

  public static func * (lhs: UnitValue, rhs: Double) -> Double { return lhs.value * rhs }
  public static func - (lhs: UnitValue, rhs: UnitValue) -> UnitValue { return UnitValue(lhs.value - rhs.value) }
  public static func / (lhs: UnitValue, rhs: UnitValue) -> UnitValue { return UnitValue(lhs.value / rhs.value) }
}

extension UnitValue: ExpressibleByIntegerLiteral {
  public init(integerLiteral value: Int) {
    self.init(Double(value))
  }
}

extension UnitValue: ExpressibleByFloatLiteral {
  public init(floatLiteral value: Double) {
    self.init(value)
  }
}

extension UnitValue: CustomStringConvertible {
  public var description: String { return value.description }
}
