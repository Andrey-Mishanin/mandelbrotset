//
//  ArrayGradientPalleteColourStorage.swift
//  MandelbrotSetCore
//
//  Created by Andrey Mishanin on 18/10/2018.
//  Copyright © 2018 Andrey Mishanin. All rights reserved.
//

public struct ArrayGradientPaletteColourStorage: GradientPaletteColourStorage {
  public func activeRangeLowerIndex(for fraction: UnitValue) -> Int {
    return coloursWithLocations.lastIndex(where: { fraction >= $0.location })!
  }
  
  public let coloursWithLocations: [ColourWithLocation]

  public init(_ coloursWithLocations: [ColourWithLocation]) {
    self.coloursWithLocations = coloursWithLocations
  }

  public subscript(position: Int) -> ColourWithLocation { return coloursWithLocations[position] }
  public func index(before i: Int) -> Int { return coloursWithLocations.index(before: i) }
  public func index(after i: Int) -> Int { return coloursWithLocations.index(after: i) }
  public var startIndex: Int { return coloursWithLocations.startIndex }
  public var endIndex: Int { return coloursWithLocations.endIndex }
}
