//
//  VectorizedRenderer.swift
//  MandelbrotSetCore
//
//  Created by Andrey Mishanin on 08/01/2019.
//  Copyright © 2019 Andrey Mishanin. All rights reserved.
//

public struct VectorizedRenderer<P: Palette>: Renderer {
  private let palette: P

  public init(palette: P) {
    self.palette = palette
  }

  public func render(_ c: ComplexNumberArray) -> [Int] {
    var z = ComplexNumberArray(Array(repeating: ℂ(0), count: c.count))
    var iters = Array(repeating: 0.0, count: c.count)
    var i = 0
    let maxIter = type(of: palette).cardinality
    repeat {
      z.squareAdd(c)
      let m = z.magnitudes()
      let mr = m.limit(to: 4 + 1e-15, replacementValue: Double(i + 1))
      for j in mr.indices {
        if (mr[j] > 0 && iters[j] == 0) {
          iters[j] = mr[j]
        }
      }
      i += 1
    } while i < maxIter

    return iters.map { $0 > 0 ? Int($0) : maxIter }
  }

  public func traceback(for cs: [ℂ]) -> [TracebackInfo] {
    let c = ComplexNumberArray(cs)
    var z = ComplexNumberArray(Array(repeating: ℂ(0), count: c.count))
    var zs = [z]
    var mags = [Array(repeating: 0.0, count: c.count)]
    var iters = Array(repeating: 0.0, count: c.count)
    var i = 0
    let maxIter = type(of: palette).cardinality
    repeat {
      z.squareAdd(c)
      zs.append(z)
      let m = z.magnitudes()
      mags.append(m)
      let mr = m.limit(to: 4 + 1e-15, replacementValue: Double(i + 1))
      for j in mr.indices {
        if (mr[j] > 0 && iters[j] == 0) {
          iters[j] = mr[j]
        }
      }
      i += 1
    } while i < maxIter

    let res = iters.map { $0 > 0 ? Int($0) : maxIter }

    return cs.indices.map { i in
      TracebackInfo(series: zs.map { ℂ($0.reals[i], i: $0.ims[i]) },
                    magnitudes: mags.map { $0[i] },
                    result: res[i])
    }
  }
}
