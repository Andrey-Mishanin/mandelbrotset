//
//  GradientPaletteColourStorage.swift
//  MandelbrotSetCore
//
//  Created by Andrey Mishanin on 18/10/2018.
//  Copyright © 2018 Andrey Mishanin. All rights reserved.
//

public protocol GradientPaletteColourStorage: BidirectionalCollection where Element == ColourWithLocation, Index == Int {
  func activeRangeLowerIndex(for fraction: UnitValue) -> Int
}

