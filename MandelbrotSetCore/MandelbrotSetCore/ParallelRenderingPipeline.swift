//
//  ParallelRenderingPipeline.swift
//  MandelbrotSetCore
//
//  Created by Andrey Mishanin on 24/12/2018.
//  Copyright © 2018 Andrey Mishanin. All rights reserved.
//

import Foundation

public typealias PipelineFactory<Pipeline> = (DomainParams, Pipeline.Output, @escaping (Pipeline.Output) -> Void) -> Pipeline where Pipeline: RenderingPipelineProtocol

public final class ParallelRenderingPipeline<Worker, Output>
where Worker: RenderingPipelineProtocol, Output: RenderOutput, Worker.Output == PartialRenderOutput<Output> {
  public typealias Factory = PipelineFactory<Worker>
  public typealias Completion = (Output) -> Void

  private let domainParams: DomainParams
  private let output: Output
  private let completion: Completion
  private let pipelineFactory: Factory
  private let numPartitionsX: Int
  private let numPartitionsY: Int
  private let callCompletionWithIntermediateResults: Bool

  public init(domainParams: DomainParams,
              output: Output,
              completion: @escaping Completion,
              pipelineFactory: @escaping Factory,
              numPartitionsX: Int,
              numPartitionsY: Int,
              callCompletionWithIntermediateResults: Bool) {
    self.domainParams = domainParams
    self.output = output
    self.completion = completion
    self.pipelineFactory = pipelineFactory
    self.numPartitionsX = numPartitionsX
    self.numPartitionsY = numPartitionsY
    self.callCompletionWithIntermediateResults = callCompletionWithIntermediateResults
  }

  public func run() {
    let dps = domainParams.partitionedInto(numX: numPartitionsX, numY: numPartitionsY)
    let outputs = output.partitionInto(numX: numPartitionsX, numY: numPartitionsY)
    var numPartitions = numPartitionsX * numPartitionsY
    let pipelines = zip(dps, outputs).map {
      zip($0.0, $0.1).map {
        pipelineFactory($0.0, $0.1, { _ in
          numPartitions -= 1
          if self.callCompletionWithIntermediateResults || numPartitions == 0 {
            self.completion(self.output)
          }
        })
      }
    }.flatMap { $0 }

    pipelines.forEach { $0.run() }
  }
}
