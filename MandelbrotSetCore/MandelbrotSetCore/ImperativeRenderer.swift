//
//  ImperativeRenderer.swift
//  MandelbrotSetCore
//
//  Created by Andrey Mishanin on 29/08/2018.
//  Copyright © 2018 Andrey Mishanin. All rights reserved.
//

public struct ImperativeRenderer<P: Palette>: Renderer {
  private let palette: P

  public init(palette: P) {
    self.palette = palette
  }

  private func render(_ c: ℂ) -> Int {
    var z = ℂ(0)
    var i = 0
    var exponentialSum = 0.0
    let maxIter = type(of: palette).cardinality
    repeat {
      if exponentialSum > Double(maxIter) {
        exponentialSum = Double(maxIter)
      } else {
        exponentialSum += exp(-z.magnitude)
      }
      z = z * z + c
      i += 1
    } while z.magnitude <= 4 && i < maxIter
    return Int(exponentialSum)
  }

  public func render(_ c: ComplexNumberArray) -> [Int] {
    return zip(c.reals, c.ims).map({ ℂ($0, i: $1) }).map(render)
  }

  private func traceback(for c: ℂ) -> TracebackInfo {
    var z = ℂ(0)
    var zs = [z]
    var mags = [0.0]
    var i = 0
    let maxIter = type(of: palette).cardinality
    repeat {
      z = z * z + c
      zs.append(z)
      mags.append(z.magnitude)
      i += 1
    } while z.magnitude <= 4 && i < maxIter
    return TracebackInfo(series: zs, magnitudes: mags, result: i)
  }

  public func traceback(for cs: [ℂ]) -> [TracebackInfo] {
    return cs.map(traceback)
  }
}
