//
//  RecursivePaletteColourStorageTests.swift
//  MandelbrotSetCoreTests
//
//  Created by Andrey Mishanin on 19/09/2018.
//  Copyright © 2018 Andrey Mishanin. All rights reserved.
//

import XCTest
import MandelbrotSetCore

final class RecursivePaletteColourStorageTests: XCTestCase {
  func test_WhenCreatedWithTwoColours_ReturnsSecondColourAtSecondIndex() {
    let s = RecursivePaletteColourStorage(colours: colours, numStops: 3)

    let expected = ColourWithLocation(colour: colours[1], location: UnitValue(0.5))
    XCTAssertEqual(s[1], expected)
  }

  func test_WhenCreatedWithTwoColours_ReturnsFirstColourAtFirstIndex() {
    let s = RecursivePaletteColourStorage(colours: colours, numStops: 3)

    let expected = ColourWithLocation(colour: colours[0], location: 0)
    XCTAssertEqual(s[0], expected)
  }

  func test_WhenCreatedWithTwoColoursAndEvenNumberOfStops_ReturnsSecondColourAtLastValidIndex() {
    let s = RecursivePaletteColourStorage(colours: colours, numStops: 2)

    let expected = ColourWithLocation(colour: colours[1], location: 1)
    XCTAssertEqual(s[s.index(before: s.endIndex)], expected)
  }

  func test_WhenCreatedWithTwoColoursAndOddNumberOfStops_ReturnsSecondColourAtLastValidIndex() {
    let s = RecursivePaletteColourStorage(colours: colours, numStops: 1)

    let expected = ColourWithLocation(colour: colours[0], location: 1)
    XCTAssertEqual(s[s.index(before: s.endIndex)], expected)
  }

  func test_CyclesThroughThreeColours() {
    let s = RecursivePaletteColourStorage(colours: [.red, .black, .blue], numStops: 4)

    let expected: [ColourWithLocation] = [
      .init(colour: .red, location: 0),
      .init(colour: .black, location: 0.5),
      .init(colour: .blue, location: 0.75),
      .init(colour: .red, location: 1),
    ]
    XCTAssertEqual(Array(s), expected)
  }
}

private let colours: [RGBAColour] = [
  .white,
  .black,
]
