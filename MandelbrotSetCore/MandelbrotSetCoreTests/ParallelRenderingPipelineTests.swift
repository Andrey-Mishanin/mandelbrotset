//
//  ParallelRenderingPipelineTests.swift
//  MandelbrotSetCoreTests
//
//  Created by Andrey Mishanin on 24/12/2018.
//  Copyright © 2018 Andrey Mishanin. All rights reserved.
//

import XCTest
import MandelbrotSetCore

final class ParallelRenderingPipelineTests: XCTestCase {
  private let dp = DomainParams(bounds: Bounds(x: -2..<2, y: -2..<2),
                                samples: Samples(x: 6, y: 4))
  private let base = TextOutput(width: 6, height: 4)

  func test_WhenRun_InvokesFactoryWithPartitionedDomain() {
    var partialDPs = [DomainParams]()
    let p = ParallelRenderingPipeline(
      domainParams: dp,
      output: base,
      completion: { _ in },
      pipelineFactory: { (dp, o, c) -> PipelineSpy in
        partialDPs.append(dp)
        return PipelineSpy(output: o, completion: c)
    },
      numPartitionsX: 2,
      numPartitionsY: 2,
      callCompletionWithIntermediateResults: true
    )

    p.run()

    let expectedBounds = [
      Bounds(x: -2..<0, y: -2..<0),
      Bounds(x: 0..<2, y: -2..<0),
      Bounds(x: -2..<0, y: 0..<2),
      Bounds(x: 0..<2, y: 0..<2)
    ]
    let expectedSamples = Array(repeating: Samples(x: 3, y: 2), count: 4)
    let expected = zip(expectedBounds, expectedSamples).map { DomainParams(bounds: $0.0, samples: $0.1) }
    XCTAssertEqual(partialDPs, expected)
  }

  func test_WhenRun_InvokesFactoryWithPartitionedOutput() {
    var outputs = [PipelineSpy.Output]()
    let p = ParallelRenderingPipeline(
      domainParams: dp,
      output: base,
      completion: { _ in },
      pipelineFactory: { (_, o, c) -> PipelineSpy in
        outputs.append(o)
        return PipelineSpy(output: o, completion: c)
    },
      numPartitionsX: 2,
      numPartitionsY: 2,
      callCompletionWithIntermediateResults: true
    )

    p.run()

    let expected = [
      (x: 0, y: 0),
      (x: 3, y: 0),
      (x: 0, y: 2),
      (x: 3, y: 2),
      ].map { PartialRenderOutput(base: base, offsetX: $0.x, offsetY: $0.y, width: 3, height: 2) }
    XCTAssertEqual(outputs, expected)
  }

  func test_WhenRun_RunsAllPipelines() {
    var pipelines = [PipelineSpy]()
    let p = ParallelRenderingPipeline(
      domainParams: dp,
      output: base,
      completion: { _ in },
      pipelineFactory: { (_, o, c) -> PipelineSpy in
        let p = PipelineSpy(output: o, completion: c)
        pipelines.append(p)
        return p
    },
      numPartitionsX: 2,
      numPartitionsY: 2,
      callCompletionWithIntermediateResults: true
    )

    p.run()

    XCTAssert(pipelines.allSatisfy { $0.didRun })
  }

  func test_WhenWorkerCompletes_CallsOverallCompletion() {
    var pipelines = [PipelineSpy]()
    var numCompletionCalls = 0
    let p = ParallelRenderingPipeline(
      domainParams: dp,
      output: base,
      completion: { _ in numCompletionCalls += 1 },
      pipelineFactory: { (_, o, c) -> PipelineSpy in
        let p = PipelineSpy(output: o, completion: c)
        pipelines.append(p)
        return p
    },
      numPartitionsX: 2,
      numPartitionsY: 2,
      callCompletionWithIntermediateResults: true
    )

    p.run()
    pipelines.forEach { $0.complete() }

    XCTAssertEqual(numCompletionCalls, pipelines.count)
  }

  func test_WhenShouldNotCallCompletionForIntermediateResults_CallsCompletionWhenAllWorkersComplete() {
    var pipelines = [PipelineSpy]()
    var numCompletionCalls = 0
    let p = ParallelRenderingPipeline(
      domainParams: dp,
      output: base,
      completion: { _ in numCompletionCalls += 1 },
      pipelineFactory: { (_, o, c) -> PipelineSpy in
        let p = PipelineSpy(output: o, completion: c)
        pipelines.append(p)
        return p
    },
      numPartitionsX: 2,
      numPartitionsY: 2,
      callCompletionWithIntermediateResults: false
    )

    p.run()
    pipelines.forEach { $0.complete() }

    XCTAssertEqual(numCompletionCalls, 1)
  }
}

private final class PipelineSpy: RenderingPipelineProtocol {
  typealias P = ASCIIPalette
  typealias Output = PartialRenderOutput<TextOutput>

  var didRun = false
  private let output: Output
  private let completion: (Output) -> Void

  init(output: Output, completion: @escaping (Output) -> Void) {
    self.output = output
    self.completion = completion
  }

  func run() {
    didRun = true
  }

  func complete() {
    completion(output)
  }
}
