//
//  DoubleArrayExtensionsTests.swift
//  MandelbrotSetCoreTests
//
//  Created by Andrey Mishanin on 20/01/2019.
//  Copyright © 2019 Andrey Mishanin. All rights reserved.
//

import XCTest
import MandelbrotSetCore

final class DoubleArrayExtensionsTests: XCTestCase {
  func test_ArrayLimit() {
    let a: [Double] = [2, 4, 1, 6]

    XCTAssertEqual(a.limit(to: 4, replacementValue: 32), [-32, 32, -32, 32])
  }

  func test_ArrayThreshold() {
    let a: [Double] = [-32, 32, -32, 32]

    XCTAssertEqual(a.threshold(limit: 0), [0, 32, 0, 32])
  }
}
