//
//  GradientPaletteTests.swift
//  MandelbrotSetCoreTests
//
//  Created by Andrey Mishanin on 08/09/2018.
//  Copyright © 2018 Andrey Mishanin. All rights reserved.
//

import XCTest
import MandelbrotSetCore

final class GradientPaletteTests: XCTestCase {
  func test_TwoColourGradient_InterpolatesBetweenColours() {
    let p = GradientPalette(coloursWithLocations: ArrayGradientPaletteColourStorage([
      .init(colour: .red, location: UnitValue(0)),
      .init(colour: .blue, location: UnitValue(1)),
    ]))

    let x = cardinality / 2

    let expected = RGBAColour(red: UInt8(x),
                              green: 0,
                              blue: UInt8.max - UInt8(x),
                              alpha: .max)
    XCTAssertEqual(p.colorise(x), expected)
  }

  func test_ThreeColourGradient_InterpolatesBetweenColoursOfActiveSegment() {
    let purple = RGBAColour(red: .max, green: 0, blue: .max, alpha: .max)
    let yellow = RGBAColour(red: .max, green: .max, blue: 0, alpha: .max)
    let p = GradientPalette(coloursWithLocations: ArrayGradientPaletteColourStorage([
      .init(colour: purple, location: UnitValue(0)),
      .init(colour: yellow, location: UnitValue(0.6)),
      .init(colour: .blue, location: UnitValue(0.9)),
      .init(colour: .black, location: UnitValue(1)),
    ]))

    let x = 3 * cardinality / 4
    let midPointBetweenLastTwoColours = cardinality / 2

    let expected = RGBAColour(red: UInt8(midPointBetweenLastTwoColours),
                              green: UInt8(midPointBetweenLastTwoColours),
                              blue: UInt8.max - UInt8(midPointBetweenLastTwoColours) - 1, // Because floating point
                              alpha: .max)
    XCTAssertEqual(p.colorise(x), expected)
  }

  func test_ColorisingHighestValue_ReturnsLastColour() {
    let p = GradientPalette(coloursWithLocations: ArrayGradientPaletteColourStorage([
      .init(colour: .red, location: UnitValue(0)),
      .init(colour: .blue, location: UnitValue(1)),
      ]))

    let x = cardinality

    XCTAssertEqual(p.colorise(x), .blue)
  }

  func test_WhenColoursAreBlackAndWhite_MatchesGreyscalePalette() {
    let p = GradientPalette(coloursWithLocations: ArrayGradientPaletteColourStorage([
      .init(colour: .black, location: UnitValue(0)),
      .init(colour: .white, location: UnitValue(1)),
      ]))
    let referencePalette = GreyscalePalette()

    for x in 1...GreyscalePalette.cardinality {
      XCTAssertEqual(p.colorise(x), referencePalette.colorise(x))
    }
  }
}

private let cardinality = GradientPalette<ArrayGradientPaletteColourStorage>.cardinality
