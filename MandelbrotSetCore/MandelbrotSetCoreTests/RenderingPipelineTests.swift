//
//  RenderingPipelineTests.swift
//  MandelbrotSetCoreTests
//
//  Created by Andrey Mishanin on 26/09/2018.
//  Copyright © 2018 Andrey Mishanin. All rights reserved.
//

import XCTest
import MandelbrotSetCore

final class RenderingPipelineTests: XCTestCase {
  private let output = TextOutput(width: width, height: height)
  private let delayedRenderRunner = DelayedRunner()

  func test_OnlyRendersWhenRenderRunnerIsInvoked() {
    let pipeline = RenderingPipeline(domainParams: domainParams,
                                     palette: ASCIIPalette(),
                                     output: output,
                                     completion:{ _ in },
                                     renderRunner: delayedRenderRunner.run,
                                     completionRunner: { _ in })

    pipeline.run()

    XCTAssert(output.isEmpty)

    delayedRenderRunner.execute()

    XCTAssertEqual(output.toString, expected)
  }

  func test_AfterRendering_InvokesCompletionWithOutputUsingCompletionRunner() {
    var outputInCompletion: TextOutput!
    let delayedCompletionRunner = DelayedRunner()
    let pipeline = RenderingPipeline(domainParams: domainParams,
                                     palette: ASCIIPalette(),
                                     output: output,
                                     completion:{ outputInCompletion = $0 },
                                     renderRunner: delayedRenderRunner.run,
                                     completionRunner: delayedCompletionRunner.run)

    pipeline.run()

    XCTAssertNil(delayedCompletionRunner.f)

    delayedRenderRunner.execute()

    XCTAssertNil(outputInCompletion)

    delayedCompletionRunner.execute()

    XCTAssert(outputInCompletion === output)
  }
}

final class PartialRenderOutputTests: XCTestCase {
  func test_IndexTransformations() {
    let base = TextOutput(width: 4, height: 4)
    let po = PartialRenderOutput(base: base, offsetX: 1, offsetY: 1, width: 3, height: 2)

    po.set(colour: "@", at: 4)

    XCTAssertEqual(base.getColour(at: 2 * 4 + 2), "@")
  }

  func test_EvenPartitioning() {
    let base = TextOutput(width: 6, height: 4)

    let p = base.partitionInto(numX: 2, numY: 2)
    p.flatMap { $0 }.forEach { $0.set(colour: "@", at: 5) }

    [8, 11, 20, 23].forEach {
      XCTAssertEqual(base.getColour(at: $0), "@")
    }
  }
}

private let width = 64
private let height = 32
private let domainParams = DomainParams(bounds: Bounds(x: -2..<2, y: -2..<2),
                                        samples: Samples(x: width, y: height))

final class TextOutput: RenderOutput {
  let width: Int
  let height: Int
  private var data: [Character] = []

  init(width: Int, height: Int) {
    self.width = width
    self.height = height
    data = Array<Character>(repeating: " ", count: width * height)
  }

  func getColour(at index: Int) -> Character {
    return data[index]
  }

  func set(colour: Character, at index: Int) {
    data[index] = colour
  }

  var isEmpty: Bool {
    return toString.allSatisfy { $0 == " " || $0 == "\n" }
  }

  var toString: String {
    return data.lazy.chunked(size: width).map({ String($0) }).joined(separator: "\n")
  }
}

private final class ColourOutput: RenderOutput {
  fileprivate let width: Int
  fileprivate let height: Int

  init(width: Int, height: Int) {
    self.width = width
    self.height = height
  }
  func set(colour: RGBAColour, at index: Int) {}
}

private final class DelayedRunner {
  var f: (() -> Void)!
  func run(_ f: @escaping () -> Void) { self.f = f }
  func execute() { f() }
}

private let expected =
"""
································.·······························
·····················.......................····················
·················...............................················
··············.....................................·············
···········...........................................··········
·········...............................................········
········.........,,,,,,,,,,,,,,,.........................·······
······......,,,,,,,,,,,,,:::;|;:::,,,......................·····
·····....,,,,,,,,,,,,,::::;;|(*@@::::,,,....................····
····..,,,,,,,,,,,,,::::::;;|*O@@[|;;:::,,,,..................···
···..,,,,,,,,,,,,:::::;|||!(@@@@@(!|;;::,,,,..................··
··.,,,,,,,,,,,::::;;;|[@@0@@@@@@@@@$@$O;:,,,,,.................·
··,,,,,,,,,::;;;;;;||[0@@@@@@@@@@@@@@@[|;:,,,,,................·
·,,,,,:::;;|%(!((!!((@@@@@@@@@@@@@@@@@@@;::,,,,,................
·,:::::;;;|![@@@@@@O*@@@@@@@@@@@@@@@@@@*;::,,,,,................
·::::;||!(OO@@@@@@@@@@@@@@@@@@@@@@@@@@@|;::,,,,,................
@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@O!|;::,,,,,,...............
·::::;||!(OO@@@@@@@@@@@@@@@@@@@@@@@@@@@|;::,,,,,................
·,:::::;;;|![@@@@@@O*@@@@@@@@@@@@@@@@@@*;::,,,,,................
·,,,,,:::;;|%(!((!!((@@@@@@@@@@@@@@@@@@@;::,,,,,................
··,,,,,,,,,::;;;;;;||[0@@@@@@@@@@@@@@@[|;:,,,,,................·
··.,,,,,,,,,,,::::;;;|[@@0@@@@@@@@@$@$O;:,,,,,.................·
···..,,,,,,,,,,,,:::::;|||!(@@@@@(!|;;::,,,,..................··
····..,,,,,,,,,,,,,::::::;;|*O@@[|;;:::,,,,..................···
·····....,,,,,,,,,,,,,::::;;|(*@@::::,,,....................····
······......,,,,,,,,,,,,,:::;|;:::,,,......................·····
········.........,,,,,,,,,,,,,,,.........................·······
·········...............................................········
···········...........................................··········
··············.....................................·············
·················...............................················
·····················.......................····················
"""

