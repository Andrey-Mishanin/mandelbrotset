//
//  LazyChunkedCollectionTests.swift
//  MandelbrotSetCoreTests
//
//  Created by Andrey Mishanin on 10/10/2018.
//  Copyright © 2018 Andrey Mishanin. All rights reserved.
//

import XCTest
import MandelbrotSetCore

final class LazyChunkedCollectionTests: XCTestCase {
  func test_WhenCountIsDivisibleByChunkSize_AllChunksAreEquallySized() {
    let c = [1, 2, 3, 4]
    let lcc = LazyChunkedCollection(base: c, chunkSize: 2)

    let expected = [c[...1], c[2...]]
    XCTAssertEqual(Array(lcc), expected)
  }

  func test_WhenCountIsNotDivisibleByChunkSize_LastChunkContainsTheRemainder() {
    let c = [1, 2, 3]
    let lcc = LazyChunkedCollection(base: c, chunkSize: 2)

    let expected = [c[...1], c[2...]]
    XCTAssertEqual(Array(lcc), expected)
  }
}
