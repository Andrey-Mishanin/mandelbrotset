//
//  MandelbrotSetCoreTests.swift
//  MandelbrotSetCoreTests
//
//  Created by Andrey Mishanin on 29/08/2018.
//  Copyright © 2018 Andrey Mishanin. All rights reserved.
//

import XCTest
import MandelbrotSetCore

final class MandelbrotSetCoreTests: XCTestCase {
  func test_Domain() {
    let domain = makeDomain(DomainParams(bounds: Bounds(x: -2..<2, y: -2..<2),
                                         samples: Samples(x: 2, y: 2)))

    let expected = [
      [
        ℂ(-2, i: -2),
        ℂ(0, i: -2),
      ],
      [
        ℂ(-2, i: 0),
        ℂ(0, i: 0),
      ],
    ]
    XCTAssertEqual(domain, expected)
  }

  func test_RangeMidpointAndSpanInitialisation() {
    XCTAssertEqual(Range(midPoint: 0.5, span: 0.5), 0.25..<0.75)
  }

  func test_RangeFractionValue() {
    let r = 0.5..<1
    XCTAssertEqual(r.value(atFraction: UnitValue(0.2)), 0.6)
  }

  func test_ZoomingIn() {
    let b = Bounds(x: -2..<2, y: -1..<1)

    let zoomed = b.zoomed(into: (x: UnitValue(0.25), y: UnitValue(0.75)))

    let expected = Bounds(y: 0..<1, aspectRatio: 2, midX: -1)
    XCTAssertEqual(zoomed, expected)
  }

  func test_ZoomingOut() {
    let b = Bounds(x: 0.5..<0.75, y: 0.25..<0.75)

    let zoomed = b.zoomedOut()

    let expected = Bounds(x: 0.375..<0.875, y: 0..<1)
    XCTAssertEqual(zoomed, expected)
  }
}

final class ComplexNumberTests: XCTestCase {
  func test_Addition() {
    let c1 = ℂ(1, i: 3)
    let c2 = ℂ(2, i: 4)

    XCTAssertEqual(c1 + c2, ℂ(3, i: 7))
  }

  func test_Multiplication() {
    let c1 = ℂ(1, i: 3)
    let c2 = ℂ(2, i: 4)

    XCTAssertEqual(c1 * c2, ℂ(-10, i: 10))
  }

  func test_MultiplyAdd() {
    let c1 = ℂ(1, i: 3)
    let c2 = ℂ(2, i: 4)

    XCTAssertEqual(c1 * c1 + c2, ℂ(-6, i: 10))
  }

  func test_Magnitude() {
    let c = ℂ(2, i: 4)

    XCTAssertEqual(c.magnitude, 20)
  }

  func test_ArrayAddition() {
    let a1 = ComplexNumberArray([ℂ(1, i: 3)])
    let a2 = ComplexNumberArray([ℂ(2, i: 4)])

    XCTAssertEqual(a1 + a2, ComplexNumberArray([ℂ(3, i: 7)]))
  }

  func test_ArrayMultiplication() {
    let a1 = ComplexNumberArray([ℂ(1, i: 3)])
    let a2 = ComplexNumberArray([ℂ(2, i: 4)])

    XCTAssertEqual(a1 * a2, ComplexNumberArray([ℂ(-10, i: 10)]))
  }

  func test_ArrayMultiplyAdd() {
    let a1 = ComplexNumberArray([ℂ(1, i: 3)])
    let a2 = ComplexNumberArray([ℂ(2, i: 4)])

    XCTAssertEqual(a1 * a1 + a2, ComplexNumberArray([ℂ(-6, i: 10)]))
    XCTAssertEqual(ComplexNumberArray.multiplyAdd(a1, a1, a2), ComplexNumberArray([ℂ(-6, i: 10)]))
  }

  func test_SquareAdd() {
    var a1 = ComplexNumberArray([ℂ(1, i: 3)])
    let a2 = ComplexNumberArray([ℂ(2, i: 4)])

    a1.squareAdd(a2)
    XCTAssertEqual(a1, ComplexNumberArray([ℂ(-6, i: 10)]))
  }

  func test_ArrayMagnitudes() {
    let a = ComplexNumberArray([ℂ(2, i: 4)])

    XCTAssertEqual(a.magnitudes(), [20])
  }
}

final class DomainPartitioningTests: XCTestCase {
  func test_EvenPartitioning() {
    let dp = DomainParams(bounds: Bounds(x: -2..<2, y: -2..<2),
                          samples: Samples(x: 4, y: 4))

    let partitions = dp.partitionedInto(numX: 2, numY: 2)

    let expectedBounds = [
      [
        Bounds(x: -2..<0, y: -2..<0),
        Bounds(x: 0..<2, y: -2..<0),
      ],
      [
        Bounds(x: -2..<0, y: 0..<2),
        Bounds(x: 0..<2, y: 0..<2)
      ]
    ]
    let expectedSamples = [
      Array(repeating: Samples(x: 2, y: 2), count: 2),
      Array(repeating: Samples(x: 2, y: 2), count: 2)
    ]
    let expected = zip(expectedBounds, expectedSamples).map {
      zip($0.0, $0.1).map {
        DomainParams(bounds: $0.0, samples: $0.1)
      }
    }
    XCTAssertEqual(partitions, expected)
  }
}
